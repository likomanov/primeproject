﻿using Microsoft.EntityFrameworkCore;
using Prime.Models;

namespace Prime.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
    }
}
