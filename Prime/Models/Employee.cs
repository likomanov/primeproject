﻿namespace Prime.Models
{
    public class Employee
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }            
        public int MonthlySalary { get; set; }
        public string AssignedTask { get; set; }
    }
}
