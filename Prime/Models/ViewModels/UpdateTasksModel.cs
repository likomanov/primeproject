﻿namespace Prime.Models.ViewModels
{
    public class UpdateTasksModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Assignee { get; set; }
    }
}

