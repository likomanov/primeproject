﻿namespace Prime.Models.ViewModels
{
    public class AddTasksModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string? Assignee { get; set; }
    }
}
