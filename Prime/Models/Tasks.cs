﻿namespace Prime.Models
{
    public class Tasks
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Employee Assignee { get; set; }
        public int EmployeeId { get; set; }
    }
}
