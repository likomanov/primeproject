﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prime.Data;
using Prime.Models;
using Prime.Models.ViewModels;


namespace Prime.Services.Employees
{
    public class EmployeesService : IEmployeesService
    {
        private readonly ApplicationDbContext context;

        public EmployeesService(ApplicationDbContext context)
        {
            this.context = context;
        }
        public async Task Add(AddEmployeeModel addEmployeeRequest)
        {
            var employee = new Employee() // convertion for the Db
            {
                Id = Guid.NewGuid(),
                Name = addEmployeeRequest.Name,
                Email = addEmployeeRequest.Email,
                PhoneNumber = addEmployeeRequest.PhoneNumber,
                MonthlySalary = addEmployeeRequest.MonthlySalary,
                AssignedTask = addEmployeeRequest.AssignedTask,
                DateOfBirth = addEmployeeRequest.DateOfBirth
            };

            await context.Employees.AddAsync(employee); //uses the injection of ApplicationDbContext to add employee
            await context.SaveChangesAsync(); //EntityFrameWork saves the changes to the database
        }

        public async Task<List<Employee>> Index()
        {
            var employees = await context.Employees.ToListAsync();
            return employees;
        }
    }
}
