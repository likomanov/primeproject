﻿using Microsoft.AspNetCore.Mvc;
using Prime.Models;
using Prime.Models.ViewModels;

namespace Prime.Services.Employees
{
    public interface IEmployeesService
    {
        Task Add(AddEmployeeModel addEmployeeRequest);
        Task<List<Employee>> Index();  
    }
}
