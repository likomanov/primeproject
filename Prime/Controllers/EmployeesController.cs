﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prime.Data;
using Prime.Models;
using Prime.Models.ViewModels;
using Prime.Services.Employees;

namespace Prime.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly ApplicationDbContext context;
        private readonly IEmployeesService employeesService;

        public EmployeesController(ApplicationDbContext context, IEmployeesService employeesService)
        {
            this.context = context;
            this.employeesService = employeesService;
        }

        [HttpGet]
        public async Task<IActionResult> Index() // REST patters
        {
            var employees = await this.employeesService.Index();          
         
            return View(employees);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddEmployeeModel addEmployeeRequest)
        {
            await this.employeesService.Add(addEmployeeRequest);
            return RedirectToAction("Index");// redirects to the list with all Employees
        }

        [HttpGet]
        public async Task<IActionResult> View(Guid id) //"quick and dirty" pattern
        {
            var employee = await context.Employees.FirstOrDefaultAsync(x => x.Id == id);

            if (employee != null)
            {
                var viewModel = new UpdateEmployeeModel()
                {
                    Id = employee.Id,
                    Name = employee.Name,
                    Email = employee.Email,
                    PhoneNumber = employee.PhoneNumber,
                    MonthlySalary = employee.MonthlySalary,
                    AssignedTask = employee.AssignedTask,
                    DateOfBirth = employee.DateOfBirth
                };
                return await Task.Run(() => View("View", viewModel));
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> View(UpdateEmployeeModel model)
        {
            //Update the information for Employee
            var employee = await context.Employees.FindAsync(model.Id);

            if (employee != null)
            {
                employee.Name = model.Name;
                employee.Email = model.Email;
                employee.PhoneNumber = model.PhoneNumber;
                employee.MonthlySalary = model.MonthlySalary;
                employee.AssignedTask = model.AssignedTask;
                employee.DateOfBirth = model.DateOfBirth;

                await context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(UpdateEmployeeModel model)
        {
            var employee = await context.Employees.FindAsync(model.Id);

            if (employee != null)
            {
                context.Employees.Remove(employee);
                await context.SaveChangesAsync();

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
