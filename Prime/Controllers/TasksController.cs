﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prime.Data;
using Prime.Models.ViewModels;
using Prime.Models;

namespace Prime.Controllers
{
    public class TasksController : Controller
    {
        private readonly ApplicationDbContext context;

        public TasksController(ApplicationDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var tasks = await context.Tasks.ToListAsync();
            return View(tasks);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();

    }

        [HttpPost]
        public async Task<IActionResult> Add(AddTasksModel addTasksRequest)
        {
            List<Employee> empList = new List<Employee>();
            empList = await context.Employees.ToListAsync();
            ViewBag.listOfEmployees = empList;

            var task = new Tasks() // convertion for the Db
            {
                Id = Guid.NewGuid(),
                Title = addTasksRequest.Title,
                Description = addTasksRequest.Description,
            };

            await context.Tasks.AddAsync(task); //uses the injection of ApplicationDbContext to add Tasks
            await context.SaveChangesAsync(); //EntityFrameWork saves the changes to the database

            return RedirectToAction("Index");// redirects to the list with all Tasks
        }

        [HttpGet]
        public async Task<IActionResult> Update(Guid id)
        {
            var task = await context.Tasks.FirstOrDefaultAsync(x => x.Id == id);

            if (task != null)
            {
                var viewModel = new UpdateTasksModel()
                {
                    Id = task.Id,
                    Title = task.Title,
                    Description = task.Description,
           
                };
                return await Task.Run(() => View("Update", viewModel));
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Update(UpdateTasksModel model)
        {
            //Update the information for Task
            var task = await context.Tasks.FindAsync(model.Id);

            if (task != null)
            {
                task.Id = model.Id;
                task.Title = model.Title;
                task.Description = model.Description;

                await context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(UpdateTasksModel model)
        {
            var task = await context.Tasks.FindAsync(model.Id);

            if (task != null)
            {
                context.Tasks.Remove(task);
                await context.SaveChangesAsync();

                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
